import streamlit as st
import pandas as pd 
import numpy as np 
import pydeck as pdk 
import plotly.express as px
from PIL import Image
import plotly.graph_objects as go
import matplotlib.pyplot as plt
from plotly.subplots import make_subplots


data = pd.read_csv('lat_long.csv')
faixa_etaria = pd.read_csv('faixa_etaria.csv')
sexo_des = pd.read_csv('sexo_des.csv')



st.title('Dados de pessoas desaparecidas em Brasília no ano de 2020')


st.markdown('### Pessoas desaparecidas em Brasília por região administrativa')
fig = px.bar(data, x = 'REGIAO ADMINISTRATIVA', y = 'desaparecidos', height=650, color = 'desaparecidos', template = 'ggplot2')
st.write(fig) 
#pessoas_des = st.slider('Numero de pessoas desaparecidas', 2, 297)
#st.map(lat_long.query('desaparecidos >= @pessoas_des')[['lon', 'lat']])

st.markdown('### Total de pessoas desaparecidas por faixa etária, de 2017 a 2020')


fig4 = go.Figure(data=[go.Table(
    header=dict(values=list(sexo_des.columns),
                fill_color='lightskyblue',
                line_color='darkslategray',
                align='left'),
    cells=dict(values=[sexo_des['FAIXA ETARIA'], sexo_des['2017'], sexo_des['2018'], sexo_des['2019'], sexo_des['2020']],
               fill_color='lavender',
               line_color='darkslategray',
               align='left'))
])

st.write(fig4)

st.markdown('### Comparativo entre pessoas localizadas e ainda desaparecidas de acordo com a faixa etária')

#fig = px.line(faixa_etaria, x="FAIXA ETARIA", y="AINDA DESAPARECIDOS")
#fig2 = px.line(faixa_etaria, x='FAIXA ETARIA', y='LOCALIZADOS', )

fig2 = make_subplots(rows=1, cols=1)

fig2.add_trace(
    go.Scatter(x=faixa_etaria['FAIXA ETARIA'], y=faixa_etaria['LOCALIZADOS'], name='LOCALIZADOS'),
    row=1, col=1
)

fig2.add_trace(
    go.Scatter(x=faixa_etaria['FAIXA ETARIA'], y=faixa_etaria['AINDA DESAPARECIDOS'], name = 'AINDA DESAPARECIDOS'),
    row=1, col=1
)
fig2.update_layout(height=550, width=850)

st.write(fig2) 


data2 = pd.DataFrame({'TOTAL': [1280, 772]})

st.markdown('### Percentual de pessoas desaparecidas divididas pelo sexo')
fig3 = px.pie(data2, values='TOTAL', names=['MASCULINO', 'FEMININO'])
st.write(fig3)




